package com.fsail.jav1.ppvi_chores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoadingScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_screen);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        // check to see if the user is signed in
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null) {
            // if they are signed in, go to the Feed
            Intent toFeed = new Intent(this, MainActivity.class);
            startActivity(toFeed);

        } else {
            // if they are NOT signed in, go to Sign In screen
            Intent toSignIn = new Intent(this, SignIn.class);
            startActivity(toSignIn);
        }
    }
}
