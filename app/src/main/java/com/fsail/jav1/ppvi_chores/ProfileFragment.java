package com.fsail.jav1.ppvi_chores;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.JsonReader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;


public class ProfileFragment extends Fragment implements View.OnClickListener {

    private static final String PASS = "PASS";
    private static final String FAIL = "FAIL";
    private static final String NEEDS_REVIEW = "NEEDS REVIEW";
    private CheckedTextView checkedTextView;

    private final ArrayList<Chore> allJobs = new ArrayList<>();
    private final ArrayList<Chore> allHistory = new ArrayList<>();


    public static ProfileFragment newInstance() {

        Bundle args = new Bundle();

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.profile_fragment, container, false);

        TextView nameText = (TextView)inflateView.findViewById(R.id.nameText_profile);
        ImageView imageView = (ImageView)inflateView.findViewById(R.id.imageView_profile);
        ListView jobsListView = (ListView) inflateView.findViewById(R.id.jobsListView_profile);
        ListView historyListView = (ListView) inflateView.findViewById(R.id.historyListView_profile);
        Button bgCheckButton = (Button)  inflateView.findViewById(R.id.bgCheckButton);
        bgCheckButton.setOnClickListener(this);
        checkedTextView = (CheckedTextView) inflateView.findViewById(R.id.bgCheckResults_profile);
        checkedTextView.isChecked();
        checkedTextView.setText(PASS);

        // setup Tabs
        TabHost tabHost = (TabHost) inflateView.findViewById(R.id.tabHost_profile);
        tabHost.setup();

        // Jobs tab
        TabHost.TabSpec spec = tabHost.newTabSpec("JOBS");
        spec.setContent(R.id.jobs);
        spec.setIndicator("JOBS");
        tabHost.addTab(spec);

        // History tab
        spec = tabHost.newTabSpec("HISTORY");
        spec.setContent(R.id.history);
        spec.setIndicator("HISTORY");
        tabHost.addTab(spec);

        // update UI
        updateUI(nameText, imageView);

        jobsAndHistoryData();

        // create the adapter
        ChoresBaseAdapter jobsAdapter = new ChoresBaseAdapter(getContext(), allJobs);
        HistoryBaseAdapter historyAdapter = new HistoryBaseAdapter(getContext(), allHistory);
        // assign it
        jobsListView.setAdapter(jobsAdapter);
        historyListView.setAdapter(historyAdapter);
        // refresh it
        jobsAdapter.notifyDataSetChanged();
        historyAdapter.notifyDataSetChanged();

        return inflateView;
    }

    private void updateUI(TextView nameText, ImageView imageView) {
        // update name on Profile
        // get name from account
        String name = ChoresUtil.getUserName();

        if (name != null) {
            if (name.trim().length() > 0) {
                nameText.setText(name);
            } else {
                // if no name is returned, display the email instead
                nameText.setText(ChoresUtil.getUserEmail());
            }
        } else {
            // if no name is returned, display the email instead
            nameText.setText(ChoresUtil.getUserEmail());
        }

        // update profile picture
        Uri imageUri = ChoresUtil.getUserPhoto();
        if (imageUri != null) {
            // start the task to download
            AsyncTask task = new AsyncTask(imageUri.toString(), imageView);
            task.execute();
        }
        // if image is null, default image is shown
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bgCheckButton) {
            bgCheckAlertDialog();

        } else {
            FirebaseAuth.getInstance().signOut();
            // go to Sign In screen
            Intent toSignIn = new Intent(getContext(), SignIn.class);
            startActivity(toSignIn);
        }
    }

    private void jobsAndHistoryData() {
        allJobs.add(new Chore("$12", "Trim Hedges", null, null));
        allJobs.add(new Chore("$30", "Bathe Dogs", null, null));

        allHistory.add(new Chore("$12", "Trim Hedges", null, null));
        allHistory.add(new Chore("$30", "Bathe Dogs", null, null));
        allHistory.add(new Chore("$30", "Bathe Dogs", null, null));
    }

    private void bgCheckAlertDialog() {
        // create an AlertDialog
        // so the user can select a category
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Fill Out Form");
        // so the user can click away from the AlertDialog to cancel
        builder.setCancelable(true);

        builder.setView(R.layout.bgcheck_alert_layout);

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AsyncTask task = new AsyncTask(null, null);
                task.execute();
                // get results
                getResults();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // user cancelled
                // do nothing
            }
        });

        // build and show the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void getResults() {
        // pick random result
        Random rand = new Random();
        int result = rand.nextInt(3);
        Toast.makeText(getContext(), "RESULT: " + result, Toast.LENGTH_SHORT).show();

        if (result == 0) {
            checkedTextView.setText(PASS);
            checkedTextView.isChecked();
        } if (result == 1) {
            checkedTextView.setText(FAIL);
        } else if (result == 2) {
            checkedTextView.setText(NEEDS_REVIEW);
        }
    }
}

class AsyncTask extends android.os.AsyncTask <Void, Void, Void> {

    private static final String TAG = "AsyncTask";

    private final ImageView mImageView;
    private final String mImageUri;
    private Bitmap mBitmap;

    public AsyncTask(String imageUri, ImageView imageView) {
        mImageView = imageView;
        mImageUri = imageUri;
    }

    @Override
    protected Void doInBackground(Void... params) {
        if (mImageView != null) {
            try {
                // convert thumbnail (String) to URL
                URL url = new URL(mImageUri);
                // convert URL to Bitmap image
                mBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            runBackgroundCheck();
            Log.i(TAG, "doInBackground: Running Background Check...");
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (mImageView != null) {
            mImageView.setImageBitmap(mBitmap);
        }
    }

    private void runBackgroundCheck() {
        try {
            // Create URL
            URL bgCheckEndpoint = new URL("https://api.accuratebackground.com/v3/report/572d1b9a46083a09");

            // Create connection
            HttpsURLConnection myConnection =
                    (HttpsURLConnection) bgCheckEndpoint.openConnection();

            // add header to request
            myConnection.setRequestProperty("Accept",
                    "application/vnd.github.v3+json");

            // check for valid response
            if (myConnection.getResponseCode() == 200) {
                // Success
                // get reference to input stream of the connection
                InputStream responseBody = myConnection.getInputStream();

                InputStreamReader responseBodyReader =
                        new InputStreamReader(responseBody, "UTF-8");

                JsonReader jsonReader = new JsonReader(responseBodyReader);

                jsonReader.close();
                myConnection.disconnect();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
