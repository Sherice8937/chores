package com.fsail.jav1.ppvi_chores;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;


public class FeedDetailsFragment extends Fragment {

    public static final String TITLE_EXTRA = "chores_by_sas_TITLE_EXTRA";
    public static final String DESCRIPTION_EXTRA = "chores_by_sas_DESCRIPTION_EXTRA";
    public static final String PRICE_EXTRA = "chores_by_sas_PRICE_EXTRA";
    public static final String IMAGES_EXTRA = "chores_by_sas_IMAGES_EXTRA";

    public static FeedDetailsFragment newInstance() {

        Bundle args = new Bundle();

        FeedDetailsFragment fragment = new FeedDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.feed_details_fragment, container, false);

        // get access to the data sent from Feed intent
        Intent fromFeed = getActivity().getIntent();

        TextView title = (TextView) inflateView.findViewById(R.id.titleText_feedDetails);
        TextView price = (TextView) inflateView.findViewById(R.id.priceText_feedDetails);
        TextView description = (TextView) inflateView.findViewById(R.id.descriptionText_feedDetails);
        GridView gridView = (GridView) inflateView.findViewById(R.id.gridView_feedDetails);



        // display data passed in
        title.setText(fromFeed.getStringExtra(TITLE_EXTRA));
        // add $ to price
        String fullPrice = "$" + fromFeed.getStringExtra(PRICE_EXTRA);
        price.setText(fullPrice);
        description.setText(fromFeed.getStringExtra(DESCRIPTION_EXTRA));

        ArrayList<String> stringImages = fromFeed.getStringArrayListExtra(IMAGES_EXTRA);
        if (stringImages != null) {

            ArrayList<Bitmap> bitmapImages = new ArrayList<>();
            // convert the images to bitmaps
            for (int i = 0; i < stringImages.size(); i++) {
                bitmapImages.add(ChoresUtil.stringToBitmap(stringImages.get(i)));
            }

            GridViewAdapter gridViewAdapter = new GridViewAdapter(getActivity(), bitmapImages);
            gridView.setAdapter(gridViewAdapter);
        }

        return inflateView;
    }
}
