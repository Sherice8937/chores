package com.fsail.jav1.ppvi_chores;

import java.util.List;


class Chore {

    private String price;
    private String title;
    private String description;
    private List<String> images;

    public Chore(String price, String title, String description, List<String> images) {
        this.price = price;
        this.title = title;
        this.description = description;
        this.images = images;
    }

    public Chore() {
        // Default constructor required for calls to DataSnapshot.getValue(Chore.class)
    }

    public String getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getImages() {
        return images;
    }
}
