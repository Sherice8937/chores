package com.fsail.jav1.ppvi_chores;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class PostFragment extends Fragment implements View.OnClickListener {

    private DatabaseReference mDatabase;

    private static final int REQUEST_TAKE_PICTURE = 0x01001;
    // array to hold all images taken
    private final ArrayList<Bitmap> allImages = new ArrayList<>();
    private GridViewAdapter gridViewAdapter;

    private EditText title;
    private EditText description;
    private EditText price;

    private int category = 0;

    public static PostFragment newInstance() {

        Bundle args = new Bundle();

        PostFragment fragment = new PostFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.post_fragment, container, false);

        Button submit = (Button)inflateView.findViewById(R.id.submitButton_post);
        ImageButton camera = (ImageButton)inflateView.findViewById(R.id.cameraButton_post);
        submit.setOnClickListener(this);
        camera.setOnClickListener(this);

        title = (EditText)inflateView.findViewById(R.id.titleText_post);
        description = (EditText)inflateView.findViewById(R.id.descriptionText_post);
        price = (EditText)inflateView.findViewById(R.id.priceText_post);

        GridView gridView = (GridView)inflateView.findViewById(R.id.gridView_post);
        gridViewAdapter = new GridViewAdapter(getActivity(), allImages);
        gridView.setAdapter(gridViewAdapter);

        return inflateView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submitButton_post) {
            // first check if any of the fields are blank
            if (checkFields()) {
                // if they aren't blank, show an alert dialog
                // to select a category
                categoryAlertDialog();
            }
            // if they are blank, do nothing
        }

        if (v.getId() == R.id.cameraButton_post) {
            // go to built-in camera or camera roll, so user can take picture
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, getOutputUri());
            startActivityForResult(intent, REQUEST_TAKE_PICTURE);
        }
    }

    private Uri getOutputUri() {
        // get the URI to return to another application

        // create file
        File protectedStorage = getActivity().getExternalFilesDir("images");

        // provide a file to store
        File imageFile = new File(protectedStorage, "textImage.jpg");

        return FileProvider.getUriForFile(getContext(),"com.fullsail.android.choresfileprovider",imageFile);
    }

    private String getOutputFilePath() {
        // get a patch to save an image to
        // create file
        File protectedStorage = getActivity().getExternalFilesDir("images");

        // provide a file to store
        File imageFile = new File(protectedStorage, "textImage.jpg");

        return imageFile.getAbsolutePath();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        BitmapFactory.Options opt = new BitmapFactory.Options();
        // downsize image to save memory
        opt.inSampleSize = 8;

        // set bitmap image from camera result & convert to Bitmap object
        Bitmap bitmap = BitmapFactory.decodeFile(getOutputFilePath());
        allImages.add(bitmap);
        gridViewAdapter.notifyDataSetChanged();
    }

    private boolean checkFields() {
        // check if the Title field is empty
        if (title.getText().toString().trim().equals("")) {
            title.setError("Title cannot be empty");
            return false;
        }

        // check if the Price field is empty
        if (price.getText().toString().trim().equals("")) {
            price.setError("Price cannot be empty");
            return false;
        }

        // check if the Description field is empty
        if (description.getText().toString().trim().equals("")) {
            description.setError("Description cannot be empty");
            return false;
        }

        return true;
    }

    private void newChore() {
        String categoryName;
        // first check which category was chosen
        if (category == 0) {
            categoryName = "Indoors";
        } else if (category == 1) {
            categoryName = "Outdoor";
        } else if (category == 2) {
            categoryName = "Animals";
        } else {
            categoryName = "Other";
        }

        // get user email
        String userEmail = ChoresUtil.getUserEmail();

        if (userEmail == null) {
            Toast.makeText(getContext(), "There was a problem, please try again.",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        // list to hold string images
        ArrayList<String> stringImages = new ArrayList<>();

        // convert bitmap images to strings
        for (int i = 0; i < allImages.size(); i++) {
            String newImg = ChoresUtil.bitmapToString(allImages.get(i));
            stringImages.add(newImg);
        }

        // save new chore to the database
        DatabaseReference postsRef = mDatabase.child(categoryName);

        postsRef.push().setValue(new Chore(price.getText().toString(),
                title.getText().toString(), description.getText().toString(), stringImages));

        // empty the fields and alert the user that the chore was posted
        emptyFields();
    }

    private void categoryAlertDialog() {
        // create an AlertDialog
        // so the user can select a category
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Select a Category");
        // so the user can click away from the AlertDialog to cancel
        builder.setCancelable(true);

        builder.setSingleChoiceItems(R.array.spinnerEntries, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // get the category that the user clicked
                category = which;
            }
        });

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // user has selected a category
                // add this chore to the database based on category
                newChore();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // user cancelled
                // do nothing
            }
        });

        // build and show the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void emptyFields() {
        title.setText("");
        price.setText("");
        description.setText("");
        allImages.clear();
        gridViewAdapter.notifyDataSetChanged();

        Toast.makeText(getContext(), "Chore created!", Toast.LENGTH_SHORT).show();
    }
}
