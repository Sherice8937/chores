// Sherice Sutcliffe
// JAV2 - 1705
// GridViewAdapter.java

package com.fsail.jav1.ppvi_chores;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

class GridViewAdapter extends BaseAdapter {

    private static final int baseID = 0x0A0B0CD;

    // reference to owning screen
    private final Context context;
    // reference to collection
    private final ArrayList<Bitmap> allImages;

    GridViewAdapter(Context _context, ArrayList<Bitmap> _allImages) {
        context = _context;
        allImages = _allImages;
    }

    @Override
    public int getCount() {
        if (allImages != null) {
            return allImages.size();
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return baseID + position;
    }

    @Override
    public Object getItem(int position) {
        if (allImages != null && position < allImages.size() && position > -1) {
            return allImages.get(position);
        }
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.grid_view_layout, parent, false);
        }

        Bitmap image = (Bitmap) getItem(position);

        ImageView imageView = (ImageView)convertView.findViewById(R.id.imageView);
        imageView.setImageBitmap(image);

        return convertView;
    }
}
