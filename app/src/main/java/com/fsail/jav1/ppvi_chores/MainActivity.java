package com.fsail.jav1.ppvi_chores;


import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private void getLocation() {
        // get location manager
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // check for permissions
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // if we do, get last known location & check if it's a valid location
            Location lastKnown = locationManager.getLastKnownLocation
                    (LocationManager.NETWORK_PROVIDER);

            // get longitude & latitude
            if (lastKnown != null) {
                locationManager.removeUpdates(this);
            }

        } else {
            // request permissions
            ActivityCompat.requestPermissions(this,
                    new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                // continue & load fragments
                switch (item.getItemId()) {
                    case R.id.navigation_feed:
                        // change to feed fragment
                        getSupportFragmentManager().beginTransaction().replace(R.id.content,
                                FeedFragment.newInstance()).commit();
                        return true;
                    case R.id.navigation_post:
                        // change to post fragment
                        getSupportFragmentManager().beginTransaction().replace(R.id.content,
                                PostFragment.newInstance()).commit();
                        return true;
                    case R.id.navigation_messages:
                        // change to messages fragment
                        getSupportFragmentManager().beginTransaction().replace(R.id.content,
                                MessagesFragment.newInstance()).commit();
                        return true;
                    case R.id.navigation_profile:
                        // change to profile fragment
                        getSupportFragmentManager().beginTransaction().replace(R.id.content,
                                ProfileFragment.newInstance()).commit();
                        return true;
                }

            } else {
                displayLocationAlert();
            }

            return false;
        }

    };

    private void displayLocationAlert() {
        // create an AlertDialog
        // so the user can select a category
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Location Required");
        builder.setMessage("We need your location to show chores in your area.");
        // so the user cannot click away from the AlertDialog to cancel
        builder.setCancelable(false);

        builder.setPositiveButton("Allow", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // user will allow location permission, request it again
                getLocation();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // user cancelled
                // do nothing
            }
        });

        // build and show the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // activate Facebook Logging
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            // change to feed fragment
            getSupportFragmentManager().beginTransaction().replace(R.id.content,
                    FeedFragment.newInstance()).commit();
        }

        getLocation();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


}
