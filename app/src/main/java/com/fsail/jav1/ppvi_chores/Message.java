package com.fsail.jav1.ppvi_chores;


class Message {

    private final String message;
    private final String name;

    public Message(String message, String name) {
        this.message = message;
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public String getName() {
        return name;
    }

}
