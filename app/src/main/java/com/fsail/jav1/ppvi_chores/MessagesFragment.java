package com.fsail.jav1.ppvi_chores;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;


public class MessagesFragment extends Fragment implements View.OnClickListener {

    private EditText mUserInput;
    private final ArrayList<Message> mAllMessages = new ArrayList<>();
    private DatabaseReference mDatabase;
    private ListView mMessagesList;

    public static MessagesFragment newInstance() {

        Bundle args = new Bundle();

        MessagesFragment fragment = new MessagesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.messages_fragment, container, false);

        FloatingActionButton fab = (FloatingActionButton) inflateView.findViewById(R.id.fab);
        fab.setOnClickListener(this);

        mUserInput = (EditText) inflateView.findViewById(R.id.input);
        mMessagesList = (ListView) inflateView.findViewById(R.id.list_of_messages);

        queryDatabase();

        return inflateView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fab) {
            // Read the input field and push a new instance
            // of ChatMessage to the Firebase database
            DatabaseReference postsRef = mDatabase.child("Users");

            String name = ChoresUtil.getUserName();

            if (name != null) {
                // check that the email is not null or blank
                if (name.trim().length() > 0) {
                    postsRef.push().setValue(new Message(
                            mUserInput.getText().toString(),
                            name)
                    );
                } else {
                    // if the user has no name, display their email instead
                    postsRef.push().setValue(new Message(
                            mUserInput.getText().toString(),
                            ChoresUtil.getUserEmail())
                    );
                }
            } else {
                // if the user has no name, display their email instead
                postsRef.push().setValue(new Message(
                        mUserInput.getText().toString(),
                        ChoresUtil.getUserEmail())
                );
            }

            // Clear the input
            mUserInput.setText("");

            // update list
            queryDatabase();
        }
    }

    private void queryDatabase() {
        // Get a reference to our posts
        final DatabaseReference postsRef = mDatabase.child("Users");

        postsRef.addValueEventListener(postListener);
    }

    private final ValueEventListener postListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            // Get map of messages in dataSnapshot
            collectMessages((Map<String,Object>) dataSnapshot.getValue());
            // TODO: send notification
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            // Getting chore failed, log a message
            Toast.makeText(getContext(), "An error occurred, please try again later.",
                    Toast.LENGTH_SHORT).show();
        }
    };

    private void collectMessages(Map<String,Object> chores) {
        // clear the list
        mAllMessages.clear();

        // iterate through each chore, ignoring their UID
        for (Map.Entry<String, Object> entry : chores.entrySet()) {

            // get user map
            Map singleChore = (Map) entry.getValue();

            // get phone field and append to list
            mAllMessages.add(new Message(singleChore.get("message").toString(),
                    singleChore.get("name").toString()));
        }

        // now refresh the list, displaying new data
        refreshList();
    }

    private void refreshList() {

        // create the adapter
        MessagesBaseAdapter adapter = new MessagesBaseAdapter(getContext(), mAllMessages);
        // assign it
        mMessagesList.setAdapter(adapter);
        // refresh it
        adapter.notifyDataSetChanged();
    }
}
