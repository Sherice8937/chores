package com.fsail.jav1.ppvi_chores;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FeedDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_details);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.feedDetailsFrameLayout, FeedDetailsFragment.newInstance()).commit();
    }
}
