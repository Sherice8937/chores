package com.fsail.jav1.ppvi_chores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;


class MessagesBaseAdapter extends BaseAdapter {

    private static final int baseID = 0x0A0B0CD;

    // reference to owning screen
    private final Context context;
    // reference to collection
    private final ArrayList<Message> allMessages;

    // default constructor
    MessagesBaseAdapter(Context _context, ArrayList<Message> _allChores) {
        context = _context;
        allMessages = _allChores;
    }

    public int getCount() {
        if (allMessages != null) {
            return allMessages.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        if (allMessages != null && position < allMessages.size() && position > -1) {
            return allMessages.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return baseID + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MessagesBaseAdapter.ViewHolder viewHolder;

        // check for a recycled view first
        if (convertView == null) {
            // if there isn't a recycled view, create one using inflater
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.messages_format, parent, false);
            viewHolder = new MessagesBaseAdapter.ViewHolder((convertView));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MessagesBaseAdapter.ViewHolder)convertView.getTag();
        }

        // get the object for this position
        Message message = (Message) getItem(position);

        // setup the view using ViewHolder
        viewHolder.name.setText(message.getName());
        viewHolder.message.setText(message.getMessage());

        // return the view
        return convertView;
    }

    static class ViewHolder {
        // Views from layout
        public final TextView name;
        public final TextView message;

        // constructor that sets the views up
        public ViewHolder(View v) {
            name = (TextView)v.findViewById(R.id.nameText_messages);
            message = (TextView)v.findViewById(R.id.messageText_messages);
        }
    }
}
