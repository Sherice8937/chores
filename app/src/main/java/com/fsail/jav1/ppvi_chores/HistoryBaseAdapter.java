package com.fsail.jav1.ppvi_chores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;


class HistoryBaseAdapter extends BaseAdapter {

    private static final int baseID = 0x0A0B0CD;

    // reference to owning screen
    private final Context context;
    // reference to collection
    private final ArrayList<Chore> allChores;

    // default constructor
    HistoryBaseAdapter(Context _context, ArrayList<Chore> _allChores) {
        context = _context;
        allChores = _allChores;
    }

    public int getCount() {
        if (allChores != null) {
            return allChores.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        if (allChores != null && position < allChores.size() && position > -1) {
            return allChores.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return baseID + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        HistoryBaseAdapter.ViewHolder viewHolder;

        // check for a recycled view first
        if (convertView == null) {
            // if there isn't a recycled view, create one using inflater
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.history_list_layout, parent, false);
            viewHolder = new HistoryBaseAdapter.ViewHolder((convertView));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (HistoryBaseAdapter.ViewHolder)convertView.getTag();
        }

        // get the object for this position
        Chore profile = (Chore) getItem(position);

        // setup the view using ViewHolder
        viewHolder.title.setText(profile.getTitle());
        //viewHolder.imageView.setImageResource(profile.getImageID());

        // return the view
        return convertView;
    }

    static class ViewHolder {
        //TODO: IMAGEVIEW

        // Views from layout
        public final TextView title;
        //public final ImageView imageView;

        // constructor that sets the views up
        public ViewHolder(View v) {
            title = (TextView)v.findViewById(R.id.titleText_history);
            //imageView = (ImageView)v.findViewById(R.id.baseItemImg);
        }
    }
}
