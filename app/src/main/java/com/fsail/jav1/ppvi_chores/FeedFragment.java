package com.fsail.jav1.ppvi_chores;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class FeedFragment extends Fragment implements AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {

    private final ArrayList<Chore> allChores = new ArrayList<>();
    private DatabaseReference mDatabase;
    private ListView list;

    public static FeedFragment newInstance() {

        Bundle args = new Bundle();

        FeedFragment fragment = new FeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.feed_fragment, container, false);

        SearchView search = (SearchView)inflateView.findViewById(R.id.searchView);
        search.setQueryHint("Search");

        Spinner spinner = (Spinner)inflateView.findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);

        list = (ListView)inflateView.findViewById(R.id.choresListView);
        list.setOnItemClickListener(this);

        queryDatabase(0);

        return inflateView;
    }

    private void queryDatabase(int position) {
        // Get a reference to our posts
        final DatabaseReference postsRef;

        // check which category was selected
        if (position == 0) {
            // Indoors
            postsRef = mDatabase.child("Indoors");

        } else if (position == 1) {
            // Outdoor
            postsRef = mDatabase.child("Outdoor");

        } else if (position == 2) {
            // Animals
            postsRef = mDatabase.child("Animals");

        } else {
            // Other
            postsRef = mDatabase.child("Other");
        }

        postsRef.addValueEventListener(postListener);
    }

    private final ValueEventListener postListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            // Get map of chores in dataSnapshot
            collectChores((Map<String,Object>) dataSnapshot.getValue());
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            // Getting chore failed, log a message
            Toast.makeText(getContext(), "An error occurred, please try again later.",
                    Toast.LENGTH_SHORT).show();
        }
    };

    private void collectChores(Map<String,Object> chores) {
        // clear the list
        allChores.clear();

        //iterate through each chore, ignoring their UID
        for (Map.Entry<String, Object> entry : chores.entrySet()) {

            // get user map
            Map singleChore = (Map) entry.getValue();


            // get phone field and append to list
            allChores.add(new Chore(singleChore.get("price").toString(),
                    singleChore.get("title").toString(),
                    singleChore.get("description").toString(),
                    (List<String>)singleChore.get("images")));
        }

        // now refresh the list, displaying new data
        refreshList();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // query the database, because the category has changed
        queryDatabase(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void refreshList() {
        // create the adapter
        ChoresBaseAdapter adapter = new ChoresBaseAdapter(getContext(), allChores);
        // assign it
        list.setAdapter(adapter);
        // refresh it
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // get item selected
        // send data to Feed Details
        Intent toFeedDetails = new Intent(getContext(), FeedDetails.class);

        toFeedDetails.putExtra(FeedDetailsFragment.TITLE_EXTRA,
                allChores.get(position).getTitle());

        toFeedDetails.putExtra(FeedDetailsFragment.DESCRIPTION_EXTRA,
                allChores.get(position).getDescription());

        toFeedDetails.putExtra(FeedDetailsFragment.PRICE_EXTRA,
                allChores.get(position).getPrice());

        // check if images are null
        if (allChores.get(position).getImages() != null) {
            // cast the List of images to an ArrayList
            // so it can be sent through intent
            ArrayList<String> allImages = new ArrayList<>();
            for (int i = 0; i < allChores.get(position).getImages().size(); i++) {
                allImages.add(allChores.get(position).getImages().get(i));
            }

            toFeedDetails.putStringArrayListExtra(FeedDetailsFragment.IMAGES_EXTRA,
                    allImages);
        }

        startActivity(toFeedDetails);
    }
}
