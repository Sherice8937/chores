# README #

Chores
by Sherice Sutcliffe

### About ###

* Chores was created from July - August 2017 (1 month approx.)
* It allows users to post small jobs they need done around the house, and lets other users see them and complete them to earn money
* DISCLAIMER: This application was created in a short time frame and is prone to bugs and may feel incomplete

### Set Up Requirements ###

* Up-To-Date Google Play Services
* Internet/WiFi
* 21+ API

### Contact ###

* Repo owner/admin: Sherice Sutcliffe
* Email: sherice.sutcliffe8937@yahoo.com

## Please contact me if you wish to download/use this project in any way